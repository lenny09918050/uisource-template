import * as configmodule from "./configmodule"
import * as debugmodule from "./debugmodule"

export Modules = {
    configmodule,
    debugmodule
}

